﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;
using MoreMountains.CorgiEngine;

public class WaitAndDo : MonoBehaviour {

    public void WaitDo()
    {
        StartCoroutine(waitsecond());
    }

    public void SetOff()
    {
        gameObject.SetActive(false);
    }

    public IEnumerator waitsecond()
    {
        yield return new WaitForSeconds(2.0f);
        gameObject.GetComponent<DOTweenPath>().DOPlay();
        yield return new WaitForSeconds(3.0f);
        SetOff();
    }
}
