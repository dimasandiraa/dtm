﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class SpawnCheckpoint : MonoBehaviour
{

    public GameObject obstacle;
    private BoxCollider2D _collider;

    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _collider.enabled = false;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            _collider.enabled = true;
            StartCoroutine(ObstacleOff());
        }
    }

    public IEnumerator ObstacleOff()
    {
        yield return new WaitForSeconds(3.0f);
        _collider.enabled = false;
    }
}
