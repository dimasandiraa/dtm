﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class ButtonActivationObs : ButtonActivated {

    public GameObject obstacle;
    private SpriteRenderer _renderer;
    private Collider2D _collider;

    private void Start()
    {
        _renderer = obstacle.GetComponent<SpriteRenderer>();
        _collider = obstacle.GetComponent<Collider2D>();
        _renderer.enabled = false;
        _collider.enabled = false;
    }

    public override void TriggerButtonAction()
    {
        PathMovement speed = obstacle.GetComponent<PathMovement>();
        if (speed != null)
        {
            speed.changeSpeed();
        }
        _renderer.enabled = true;
        _collider.enabled = true;
        StartCoroutine(ObstacleOff());
    }

    public IEnumerator ObstacleOff()
    {
        yield return new WaitForSeconds(3.0f);
        _renderer.enabled = false;
        _collider.enabled = false;
    }

}

