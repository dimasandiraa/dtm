﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class SpawnObstacle : MonoBehaviour
{

    public GameObject obstacle;
    public GameObject player;
    public GameObject SlowMotion;

    public float showSaw = 3.0f;
    public float timeOff = 3.0f;
    public bool isFreeze = false;

    private SpriteRenderer _renderer;
    private Collider2D _collider;

    private CharacterHorizontalMovement _speed;

    private void Start()
    {
        player = GameObject.Find("Rectangle");

        _renderer = obstacle.gameObject.GetComponent<SpriteRenderer>();
        _collider = obstacle.gameObject.GetComponent<Collider2D>();
        _speed = player.GetComponent<CharacterHorizontalMovement>();

        if(_renderer != null)
        {
            _renderer.enabled = false;
        }
        if(_collider != null)
        {
            _collider.enabled = false;
        }
        if (SlowMotion != null)
        {
            SlowMotion.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(player.GetComponent<PlayerTriggerer>().startChestT)
            {
                StartCoroutine(startObstacle());
            }
            else
            {
                if (_renderer != null)
                {
                    _renderer.enabled = true;
                }
                if (_collider != null)
                {
                    _collider.enabled = true;
                }
                if (isFreeze == true)
                {
                    LevelManager.Instance.FreezeCharacters();
                }
                if (SlowMotion != null)
                {
                    SlowMotion.gameObject.SetActive(true);
                }
            }
            StartCoroutine(ObstacleOff());
        }
    }

    public IEnumerator ObstacleOff()
    {
        yield return new WaitForSeconds(timeOff);
        Debug.Log("TEST");
        if (_renderer != null)
        {
            _renderer.enabled = false;
        }
        if (_collider != null)
        {
            _collider.enabled = false;
        }
        if (SlowMotion != null)
        {
            SlowMotion.gameObject.SetActive(false);
        }
    }

    public IEnumerator startObstacle()
    {
        if (isFreeze == true)
        {
            LevelManager.Instance.FreezeCharacters();
        }
        
        yield return new WaitForSeconds(showSaw);

        obstacle.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        obstacle.gameObject.GetComponent<Collider2D>().enabled = true;
    }
}
