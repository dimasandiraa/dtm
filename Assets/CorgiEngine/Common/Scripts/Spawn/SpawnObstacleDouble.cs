﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class SpawnObstacleDouble : MonoBehaviour
{

    public GameObject obstacle;
    private SpriteRenderer _renderer;
    private Collider2D _collider;
    private bool _firstTriggger = true;

    private void Start()
    {
        _renderer = obstacle.GetComponent<SpriteRenderer>();
        _collider = obstacle.GetComponent<Collider2D>();
        _renderer.enabled = false;
        _collider.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (_firstTriggger)
            {
                _renderer.enabled = true;
                _collider.enabled = true;
                _firstTriggger = false;
            }
            else
            {
                _renderer.enabled = false;
                _collider.enabled = false;
                _firstTriggger = true;
            }
        }
    }
}
