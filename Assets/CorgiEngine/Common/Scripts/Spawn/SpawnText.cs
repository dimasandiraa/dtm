﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnText : MonoBehaviour {

    public GameObject text;
 
    private void Start()
    {
        text.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        text.SetActive(true);
        StartCoroutine(TextTime());
    }

   IEnumerator TextTime()
    {
        yield return new WaitForSeconds(2*0.0001f);
        text.SetActive(false);
    }

  
}