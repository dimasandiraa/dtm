﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public class TriggerMovement : MonoBehaviour
    {

        public GameObject triggeredObject;
        public bool isDelay = false;
        public float delayTime=3.0f;
        PathMovement speed;

        private void Start()
        {
            speed = triggeredObject.GetComponent<PathMovement>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (speed != null)
                {
                    if (isDelay == true)
                    {
                        StartCoroutine(Delaying());
                    }
                    else
                    {
                        speed.changeSpeed();
                    }
                }
            }
        }

        public IEnumerator Delaying()
        {
            yield return new WaitForSeconds(delayTime);
            speed.changeSpeed();
        }
    }
}
