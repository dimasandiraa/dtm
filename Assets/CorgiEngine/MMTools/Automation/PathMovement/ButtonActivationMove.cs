﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class ButtonActivationMove : ButtonActivated {

    public GameObject triggeredObject;

    private SpriteRenderer _renderer;
    private Collider2D _collider;

    private void Start()
    {
        _renderer = triggeredObject.GetComponent<SpriteRenderer>();
        _collider = triggeredObject.GetComponent<Collider2D>();
        _renderer.enabled = true;
        _collider.enabled = true;
    }

    private void Update()
    {
        _renderer.enabled = true;
        _collider.enabled = true;
    }

    public override void TriggerButtonAction()
    {
        PathMovement speed = triggeredObject.GetComponent<PathMovement>();
        if (speed != null)
        {
            speed.changeSpeed();
        }
    }
}
