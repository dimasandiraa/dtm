﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGuideEvent1 : MonoBehaviour {

	public bool guide1 = true;
	public bool startEv1;

	void OnTriggerEnter2D(Collider2D player)
	{
		if(player.gameObject.name == "Rectangle" && guide1)
		{
			startEv1 = true;
		}
	}
}
