﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SplatterSystem.Platformer {

    public class SplashManager : MonoBehaviour {
        public float speed = 14f;
        public float acceleration = 6f;
        public float airAcceleration = 3f;
        public float wallPushPower = 0.75f;

        protected GroundState groundState;
        protected Rigidbody2D rb;
        protected Vector2 input;

        protected virtual void Awake() {
            rb = GetComponent<Rigidbody2D>();
            groundState = new GroundState(transform.gameObject);
        }

        void FixedUpdate() {
            float a = groundState.IsGround() ? acceleration : airAcceleration;
            rb.AddForce(new Vector2(((input.x*speed) - rb.velocity.x)*a, 0f));
         
            if (groundState.IsWall() && !groundState.IsGround() && Math.Abs(input.y - 1) < float.Epsilon) {
                //Add force negative to wall direction (with speed reduction).
                rb.velocity = new Vector2(-groundState.WallDirection()*speed*wallPushPower, rb.velocity.y);
            }
        }
    }
}
