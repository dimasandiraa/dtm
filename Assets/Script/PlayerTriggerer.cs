﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
	public class PlayerTriggerer : MonoBehaviour {

		public bool chestTrap = true;
		public bool startChestT;
		public bool activatedSlowTime;
		public bool slowedTimeOn;

		[Tooltip("dont change the value")]
		public float resetTime;

		public GameObject gamePolishingM;

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {

		}

		private void OnTriggerEnter2D(Collider2D player) 
		{
			if(player.gameObject.name.Contains("Trigger"))
			{
				slowedTimeOn = true;
			}
			else
			{
				slowedTimeOn = false;
			}

			if(player.gameObject.name.Contains("TimeSlower"))
			{
				activatedSlowTime = true;
			
				StartCoroutine(resetSlow());
			}

			if(player.gameObject.name == "TriggerSaw1" && chestTrap)
			{
				startChestT = true;
			}
		}

		IEnumerator resetSlow()
		{
			yield return new WaitForSeconds(resetTime);

			activatedSlowTime = false;
		}
	}
}