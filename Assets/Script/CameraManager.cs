﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class CameraManager : MonoBehaviour {
	[Header("Find Player")]
	public string playerName = "Player";
	private Transform player; 

	public GameObject camera;

	// Use this for initialization
	void Start () {
		addPlayer();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void addPlayer()
	{
		player = GameObject.Find(playerName).transform;

		camera.GetComponent<ProCamera2D>().AddCameraTarget(player);
	}
}
