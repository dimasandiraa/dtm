﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestEvent : MonoBehaviour {

	public bool startEvent = true;
	public bool eventActive;

	void OnTriggerEnter2D(Collider2D player)
	{
		if(player.gameObject.name == "Rectangle" && startEvent)
		{
			eventActive = true;
		}
	}
}
