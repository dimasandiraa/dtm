﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

namespace MoreMountains.CorgiEngine
{
	public class GamePolishingManager : MonoBehaviour {
		//Dont touch this
		public GameObject player;

		[Header("Chest Polishing")]
		[Tooltip("add CineCamera with camera in chest area")]
		public ProCamera2DCinematics cineCamera;
		[Tooltip("add CineCamera with camera in saw chest area")]
		public ProCamera2DCinematics sawChestCamera;
		[Tooltip("add text to display")]
		public GameObject[] text;
		[Tooltip("chestTextDelayS for show the text")]
		public float chestTextDelayS;
		[Tooltip("chestTextDelayH for hide the text")]
		public float chestTextDelayH;

		[Header("The Obstacle in chest")]
		[Tooltip("add the triggerer spawn object, it will detect the spawned obj")]
		public GameObject spawnObstacle;
		[Tooltip("add the triggerer spawn object, it will detect the triggerer")]
		public GameObject triggerMove;
		SpawnObstacle[] sawScript1;
		TriggerMovement[] sawScript2;

		[Header("Setting random Slow Motion")]
		[Tooltip("chance to start the slow motion, range is (0 - 1)")]
		public float slowChance;
		[Tooltip("the obstacle trigger, to trigger the obstacle")]
		public GameObject[] trigger;
		[Tooltip("reset the cooldown of the slow motion after die")]
		public float resetSlow;
		public float randNumber;

		[Header("Setting Camera for Guide Event 1")]
		public GameObject GuideSprite;
		public ProCamera2DCinematics cineCameraGuide;


		[Header("Setting Camera for polishing")]
		[Tooltip("add the camera from regular, chest, etc")]
		public GameObject[] camera;

		private PlayerTriggerer myPlayerTriggerer;
		private StartGuideEvent1 startGuideEv1;
		private ChestEvent theChestEv;

		// Use this for initialization
		void Start () {
			myPlayerTriggerer = FindObjectOfType<PlayerTriggerer>();
			startGuideEv1 = FindObjectOfType<StartGuideEvent1>();
			theChestEv = FindObjectOfType<ChestEvent>();

			player = GameObject.Find("Rectangle");

			player.GetComponent<PlayerTriggerer>().resetTime = this.resetSlow;
		}
		
		// Update is called once per frame
		void Update () {
			if(myPlayerTriggerer.startChestT)
			{
				camera[0].SetActive(false);
				camera[1].SetActive(true);

				cineCamera.Play();

				myPlayerTriggerer.chestTrap = false;
				myPlayerTriggerer.startChestT = false;

				StartCoroutine(showChestText());

				sawScript1 = spawnObstacle.GetComponents<SpawnObstacle>();

				foreach (SpawnObstacle theSaw in sawScript1)
				{
					theSaw.isFreeze = false;
					theSaw.showSaw = 1f;
					theSaw.timeOff = 8f;
				}

				sawScript2 = spawnObstacle.GetComponents<TriggerMovement>();

				foreach (TriggerMovement theMovement in sawScript2)
				{
					theMovement.delayTime = 3f;
				}	
			}

			if(myPlayerTriggerer.slowedTimeOn)
			{

				myPlayerTriggerer.slowedTimeOn = true;

				if(!myPlayerTriggerer.activatedSlowTime)
				{
					randNumber = Random.value;	
				}
			}

			meetGuideEv1();
			sawChest();
		}

		IEnumerator showChestText()
		{
			yield return new WaitForSeconds(chestTextDelayS);

			text[0].SetActive(true);

			yield return new WaitForSeconds(chestTextDelayH);

			text[0].SetActive(false);

			camera[1].SetActive(false);
			camera[0].SetActive(true);
		}

		public void setRandSlowTime()
		{
			if(randNumber < slowChance)
			{
				foreach (GameObject theTrigger in trigger)
				{
					theTrigger.SetActive(true);
				}
				
				randNumber = 0;
			}
			else
			{
				foreach (GameObject theTrigger in trigger)
				{
					theTrigger.SetActive(false);
				}
			}
		}

		public void meetGuideEv1()
		{
			if(startGuideEv1.startEv1)
			{
				player.GetComponent<CharacterHorizontalMovement>().enabled = false;	
				player.GetComponent<Animator>().enabled = false;	

				GuideSprite.SetActive(true);

				camera[0].SetActive(false);
				camera[2].SetActive(true);		
			
				LevelManager.Instance.FreezeCharacters();

				cineCameraGuide.Play();

				StartCoroutine(finishGuide1());
			}
		}

		IEnumerator finishGuide1()
		{
			yield return new WaitForSeconds(9f);

			camera[2].SetActive(false);
			camera[0].SetActive(true);

			LevelManager.Instance.UnFreezeCharacters();

			player.GetComponent<CharacterHorizontalMovement>().enabled = true;
			player.GetComponent<Animator>().enabled = true;

			GuideSprite.SetActive(false);

			startGuideEv1.guide1 = false;
			startGuideEv1.startEv1 = false;
		}

		public void sawChest()
		{
			if(theChestEv.eventActive)
			{
				player.GetComponent<CharacterHorizontalMovement>().enabled = false;	
				player.GetComponent<Animator>().enabled = false;	

				theChestEv.startEvent = false;
				theChestEv.eventActive = false;

				camera[0].SetActive(false);
				camera[3].SetActive(true);		
			
				LevelManager.Instance.FreezeCharacters();

				sawChestCamera.Play();

				StartCoroutine(finishSawChest());
			}	
		}

		IEnumerator finishSawChest()
		{
			yield return new WaitForSeconds(9f);

			text[1].SetActive(true);

			yield return new WaitForSeconds(2f);

			camera[3].SetActive(false);
			camera[0].SetActive(true);

			LevelManager.Instance.UnFreezeCharacters();

			player.GetComponent<CharacterHorizontalMovement>().enabled = true;
			player.GetComponent<Animator>().enabled = true;
		}
	}
}
