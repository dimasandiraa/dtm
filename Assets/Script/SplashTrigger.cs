﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SplatterSystem.Platformer 
{
	public class SplashTrigger : SplashManager {

		public AbstractSplatterManager splatter;

		// public float distance;
		public float paintPositionOffset = 0;

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		private void OnCollisionEnter2D(Collision2D obj) 
		{
			if(obj.gameObject.name.Contains("Box"))
			{
				if (groundState.raycastDown) {
                    splatter.Spawn(groundState.raycastDown.point + Vector2.down * paintPositionOffset, Vector3.down);

					Debug.Log("Hello");
                }
                if (groundState.raycastUp) {
                    splatter.Spawn(groundState.raycastUp.point + Vector2.up * paintPositionOffset, Vector3.up);
					Debug.Log("Hello");
                }
                if (groundState.raycastLeft) {
                    splatter.Spawn(groundState.raycastLeft.point + Vector2.left * paintPositionOffset, Vector3.left);
					Debug.Log("Hello");
                }
                if (groundState.raycastRight) {
                    splatter.Spawn(groundState.raycastRight.point + Vector2.right * paintPositionOffset, Vector3.right);
					Debug.Log("Hello");
                }
			}	
		}
	}
}
