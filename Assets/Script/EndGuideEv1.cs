﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGuideEv1 : MonoBehaviour {
	public bool endEv1;

	private StartGuideEvent1 GuideEv1;

	void Start()
	{
		GuideEv1 = FindObjectOfType<StartGuideEvent1>();
	}

	void OnTriggerEnter2D(Collider2D player)
	{
		if(player.gameObject.name == "Rectangle" && GuideEv1.startEv1)
		{
			endEv1 = true;
		}
	}
}
